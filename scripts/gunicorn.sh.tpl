#!/bin/bash

USER=root
GROUP=root
NUM_WORKERS=%GUNICORN_WORKERS%
NAME="%APPLICATION_NAME%"
DJANGO_WSGI_MODULE=%APPLICATION_NAME%.wsgi
DJANGO_SETTINGS_MODULE=%APPLICATION_NAME%.settings

DJANGODIR=$(readlink -f $(dirname ${BASH_SOURCE[0]}))
PROJECTDIR=$(readlink -f $(dirname ${DJANGODIR}))

echo "Starting $NAME"

cd $DJANGODIR
source $PROJECTDIR/ve/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

exec gunicorn ${DJANGO_WSGI_MODULE}:application \
--name $NAME \
--workers $NUM_WORKERS \
--user=$USER --group=$GROUP \
--log-level=debug \
-t 60000 \
--bind="0.0.0.0:8080"
