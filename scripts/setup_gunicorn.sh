#!/bin/bash

set -e

CURRENT_DIR=$(readlink -f $(dirname ${BASH_SOURCE[0]}))

if [[ -z "$GUNICORN_WORKERS" ]]; then
    GUNICORN_WORKERS="4"
fi

if [[ -z "$APPLICATION_NAME" || -z "$APPLICATION_REPO" ]]; then
    echo "APPLICATION_NAME and APPLICATION_REPO environment varaibles must be set"
    exit 1
fi

echo "checking the django application $APPLICATION_NAME"
if [[ ! -d $CURRENT_DIR/$APPLICATION_NAME/.git ]]; then
  echo "application not present, cloning from repository"
  if [[ -z "$SSH_KEY" && ! -f "/root/.ssh/id_rsa.pub" ]]; then
    echo "missing SSH_KEY environment variable"
    exit 1
  elif [[ ! -z "$SSH_KEY" ]]; then
    echo "$SSH_KEY" > /root/.ssh/id_rsa
    chmod 0600 /root/.ssh/id_rsa
  fi
  git clone $APPLICATION_REPO "$CURRENT_DIR/$APPLICATION_NAME"
else
  git pull origin master
fi

RUN_DIR="$CURRENT_DIR/run/"
LOGS_DIR="$CURRENT_DIR/logs/"
VIRTUAL_ENV_DIR="$CURRENT_DIR/ve/"
GUNICORN_SCRIPT="$CURRENT_DIR/$APPLICATION_NAME/gunicorn.sh"

echo "generating gunicorn script file"
sed -e "s;%GUNICORN_WORKERS%;$GUNICORN_WORKERS;g" \
    -e "s;%APPLICATION_NAME%;$APPLICATION_NAME;g" \
    $CURRENT_DIR/gunicorn.sh.tpl > $GUNICORN_SCRIPT

if [[ ! -d "$VIRTUAL_ENV_DIR" ]]; then
  echo "creating the virtual environment folder"
  virtualenv --no-site-packages $VIRTUAL_ENV_DIR
fi

echo "reproducing the project structure"
mkdir -p $RUN_DIR
mkdir -p $LOGS_DIR

echo "loading virtual environment"
source $CURRENT_DIR/ve/bin/activate
pip install -r $CURRENT_DIR/base_requirements.txt

echo "requirement check"
pip install -r $CURRENT_DIR/$APPLICATION_NAME/requirements.txt

echo "extracting static files"
cd $CURRENT_DIR/$APPLICATION_NAME
mkdir -p static
mkdir -p media
./manage.py collectstatic --noinput

chmod +x $GUNICORN_SCRIPT
