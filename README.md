dk_f20_django_1.6
=================

Docker container for django 1.6 + gunicorn + psycopg2 + some django goodies


##Info:
This Dockerfile creates a container running a gunicorn service on port 8080 for django application based on the fedora 20 distro

* expose port 8080
* initializes the django application from remote git repository (support for github/bitbucket only for now)


##Usage:

You can rebuild the container by yourself using :

```
docker build git://github.com/Chedi/dk_f20_django_1.6.git
```

or directly download it from the docker index 

```
docker pull chedi/django-1.6
```

once the image ready you can run it with something like this (see my [container_fall repo](https://github.com/Chedi/container_fall)):

```

export DATA_CONTAINER="chedi/psql-9.3:f20"
export CODE_CONTAINER="chedi/django-1.6:f20"

export APPLICATION_NAME="epsilon"
export APPLICATION_REPO="git@bitbucket.org:chedi/epsilon_comming_soon.git"

export APPLICATION_DATA_PATH="/var/data/psql"
export APPLICATION_CODE_PATH="/var/data/application"

docker pull $DATA_CONTAINER
docker pull $CODE_CONTAINER

mkdir -p "$APPLICATION_DATA_PATH"
mkdir -p "$APPLICATION_CODE_PATH"

docker run -t -d -P -v "$APPLICATION_DATA_PATH":/var/lib/pgsql "$DATA_CONTAINER"

docker run -t -d -P -v "$APPLICATION_CODE_PATH":"/var/www/project/$APPLICATION_NAME" \
-e APPLICATION_NAME="$APPLICATION_NAME" -e APPLICATION_REPO="$APPLICATION_REPO"      \
-e SSH_KEY="$(<id_rsa)" "$CODE_CONTAINER"

```

As you notice I use multiple environment variables and volume mounting to get everthing in place, this example uses also  my [postgresql container](https://github.com/Chedi/dk_f20_psql_9.3).


The database and application folders are mounted from the host machine respectively under:

* /var/data/psql
* /var/data/application

I also use my private rsa key for authenification so that must be in the same path as the script launching these commands, or if you have to have it somewhere elese modifiy the

```
-e SSH_KEY="$(<id_rsa)"
```

part of the script

##Meta:
Built with docker-io 0.7.6 on Centos 6.4
